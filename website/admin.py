from django.contrib import admin
from .models import HomePageContent, Pages, SocialMedia, Abilities


class PagesInLine(admin.TabularInline):
    model = HomePageContent.pages.through


class SocialMediaInLine(admin.TabularInline):
    model = HomePageContent.social_media.through


class AbilitiesInLine(admin.TabularInline):
    model = HomePageContent.abilities.through


class HomePageContentAdmin(admin.ModelAdmin):
    inlines = [
        PagesInLine, SocialMediaInLine, AbilitiesInLine
    ]

    def has_add_permission(self, request):
        if HomePageContent.objects.count() == 0:
            return True
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    exclude = [
        'pages', 'social_media', 'abilities'
    ]


class PagesAdmin(admin.ModelAdmin):
    def get_model_perms(self, request):
        return {}


class SocialMediaAdmin(admin.ModelAdmin):
    def get_model_perms(self, request):
        return {}


class AbilitiesAdmin(admin.ModelAdmin):
    def get_model_perms(self, request):
        return {}


admin.site.register(HomePageContent, HomePageContentAdmin)
admin.site.register(Pages, PagesAdmin)
admin.site.register(SocialMedia, SocialMediaAdmin)
admin.site.register(Abilities, AbilitiesAdmin)
