from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator


class Pages(models.Model):
    title = models.CharField(max_length=250)
    url = models.CharField(max_length=250)

    def __str__(self):
        return self.title


class SocialMedia(models.Model):
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=500)
    logo = models.CharField(max_length=50)
    color = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Abilities(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    value = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(100)])

    def __str__(self):
        return self.name


class HomePageContent(models.Model):
    title = models.CharField(max_length=250)
    pages = models.ManyToManyField(Pages)
    background_img = models.ImageField()
    profile_img = models.ImageField()
    social_media = models.ManyToManyField(SocialMedia)
    abilities = models.ManyToManyField(Abilities)

    def __str__(self):
        return "settings"
