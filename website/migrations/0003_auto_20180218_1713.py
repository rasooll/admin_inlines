# Generated by Django 2.0.2 on 2018-02-18 17:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_homepagecontent_abilities'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='homepagecontent',
            name='abilities',
        ),
        migrations.AddField(
            model_name='homepagecontent',
            name='abilities',
            field=models.ManyToManyField(to='website.Abilities'),
        ),
        migrations.RemoveField(
            model_name='homepagecontent',
            name='pages',
        ),
        migrations.AddField(
            model_name='homepagecontent',
            name='pages',
            field=models.ManyToManyField(to='website.Pages'),
        ),
        migrations.RemoveField(
            model_name='homepagecontent',
            name='social_media',
        ),
        migrations.AddField(
            model_name='homepagecontent',
            name='social_media',
            field=models.ManyToManyField(to='website.SocialMedia'),
        ),
    ]
