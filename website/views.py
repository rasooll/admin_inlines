from django.shortcuts import render
from .models import HomePageContent


def home_page(request):
    try:
        content = HomePageContent.objects.all()[0]
    except:
        content = None
    return render(request, 'index.html', context={'content': content})
